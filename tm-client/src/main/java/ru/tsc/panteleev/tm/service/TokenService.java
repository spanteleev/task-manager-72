package ru.tsc.panteleev.tm.service;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.panteleev.tm.api.service.ITokenService;

@Getter
@Setter
@Service
@NoArgsConstructor
@AllArgsConstructor
public class TokenService implements ITokenService {

    @NotNull
    private String token;

}
