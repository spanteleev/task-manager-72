package ru.tsc.panteleev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.tsc.panteleev.tm.dto.model.UserDto;

@Repository
public interface IUserDtoRepository extends JpaRepository<UserDto, String> {

    UserDto findByLogin(@NotNull String login);

}
