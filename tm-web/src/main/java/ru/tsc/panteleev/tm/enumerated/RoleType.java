package ru.tsc.panteleev.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;

@Getter
public enum RoleType {

    USUAL("Usual user"),
    ADMIN("Administrator");

    @Nullable
    private final String displayName;

    RoleType(@Nullable final String displayName) {
        this.displayName = displayName;
    }

}
