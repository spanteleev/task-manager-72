package ru.tsc.panteleev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.tsc.panteleev.tm.api.service.dto.IProjectDtoService;
import ru.tsc.panteleev.tm.api.service.dto.IProjectTaskDtoService;
import ru.tsc.panteleev.tm.api.service.dto.ITaskDtoService;
import ru.tsc.panteleev.tm.api.service.dto.IUserDtoService;
import ru.tsc.panteleev.tm.configuration.ServerConfiguration;
import ru.tsc.panteleev.tm.dto.model.ProjectDto;
import ru.tsc.panteleev.tm.dto.model.TaskDto;
import ru.tsc.panteleev.tm.dto.model.UserDto;
import ru.tsc.panteleev.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.panteleev.tm.exception.entity.TaskNotFoundException;
import ru.tsc.panteleev.tm.exception.field.ProjectIdEmptyException;
import ru.tsc.panteleev.tm.exception.field.TaskIdEmptyException;

import java.util.UUID;

public class ProjectTaskServiceTest {

    @NotNull
    private IProjectDtoService projectService;

    @NotNull
    private ITaskDtoService taskService;

    @NotNull
    private IProjectTaskDtoService projectTaskService;

    @NotNull
    private IUserDtoService userService;

    @NotNull
    private UserDto user;

    @NotNull
    private final static String STRING_RANDOM = UUID.randomUUID().toString();

    @NotNull
    private final static String STRING_EMPTY = "";

    @Before
    public void init() {
        @NotNull final AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        projectService = context.getBean(IProjectDtoService.class);
        taskService = context.getBean(ITaskDtoService.class);
        projectTaskService = context.getBean(IProjectTaskDtoService.class);
        userService = context.getBean(IUserDtoService.class);
        user = userService.create(UUID.randomUUID().toString(), UUID.randomUUID().toString(), UUID.randomUUID().toString());
    }

    @After
    public void end() {
        taskService.clear(user.getId());
        projectService.clear(user.getId());
    }

    @Test
    public void bindTaskToProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final ProjectDto project = projectService.create(user.getId(), name, description, null, null);
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final TaskDto task = taskService.create(user.getId(), taskName, taskDescription, null, null);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), STRING_RANDOM, task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.bindTaskToProject(user.getId(), project.getId(), STRING_RANDOM));
        projectTaskService.bindTaskToProject(user.getId(), project.getId(), task.getId());
        @NotNull final TaskDto taskAfterUpdate = taskService.findById(user.getId(), task.getId());
        Assert.assertSame(project.getId(), taskAfterUpdate.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final ProjectDto project = projectService.create(user.getId(), name, description, null, null);
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final TaskDto task = taskService.create(user.getId(), taskName, taskDescription, null, null);
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), STRING_EMPTY, STRING_RANDOM));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), STRING_RANDOM, STRING_EMPTY));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), STRING_RANDOM, task.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> projectTaskService.unbindTaskFromProject(user.getId(), project.getId(), STRING_RANDOM));
        projectTaskService.bindTaskToProject(user.getId(), project.getId(), task.getId());
        @NotNull TaskDto taskAfterUpdate = taskService.findById(user.getId(), task.getId());
        Assert.assertSame(project.getId(), taskAfterUpdate.getProjectId());
        projectTaskService.unbindTaskFromProject(user.getId(), project.getId(), task.getId());
        taskAfterUpdate = taskService.findById(user.getId(), task.getId());
        Assert.assertNull(taskAfterUpdate.getProjectId());
    }

    @Test
    public void removeProjectById() {
        @NotNull final String name = UUID.randomUUID().toString();
        @NotNull final String description = UUID.randomUUID().toString();
        @NotNull final ProjectDto project1 = projectService.create(user.getId(), name, description, null, null);
        @NotNull final ProjectDto project2 = projectService.create(user.getId(), name, description, null, null);
        @NotNull final String taskName = UUID.randomUUID().toString();
        @NotNull final String taskDescription = UUID.randomUUID().toString();
        @NotNull final TaskDto task1 = taskService.create(user.getId(), taskName, taskDescription, null, null);
        @NotNull final TaskDto task2 = taskService.create(user.getId(), taskName, taskDescription, null, null);
        @NotNull final TaskDto task3 = taskService.create(user.getId(), taskName, taskDescription, null, null);
        projectTaskService.bindTaskToProject(user.getId(), project1.getId(), task1.getId());
        projectTaskService.bindTaskToProject(user.getId(), project2.getId(), task2.getId());
        projectTaskService.bindTaskToProject(user.getId(), project2.getId(), task3.getId());
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.removeProjectById(user.getId(), STRING_EMPTY));
        projectTaskService.removeProjectById(user.getId(), project2.getId());
        Assert.assertNotNull(taskService.findById(user.getId(), task1.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> projectService.findById(user.getId(), project2.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.findById(user.getId(), task2.getId()));
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.findById(user.getId(), task3.getId()));
    }

}
