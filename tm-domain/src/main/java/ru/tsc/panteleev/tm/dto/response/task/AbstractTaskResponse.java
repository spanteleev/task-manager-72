package ru.tsc.panteleev.tm.dto.response.task;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.tsc.panteleev.tm.dto.response.AbstractResponse;
import ru.tsc.panteleev.tm.dto.model.TaskDto;

@Getter
@Setter
@NoArgsConstructor
public abstract class AbstractTaskResponse extends AbstractResponse {

    @Nullable
    private TaskDto task;

    public AbstractTaskResponse(@Nullable TaskDto task) {
        this.task = task;
    }

}
